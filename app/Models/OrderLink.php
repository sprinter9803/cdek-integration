<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderLink extends Model
{
    protected $table = 'order_link';

    use HasFactory;
}
