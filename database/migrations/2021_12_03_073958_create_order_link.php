<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_link', function (Blueprint $table) {
            $table->id();
            $table->string('mw_order_uuid');
            $table->string('mw_order_number')->nullable();
            $table->string('cdek_order_uuid');
            $table->text('cdek_order_request');
            $table->text('cdek_order_response');
            $table->string('status')->nullable();
            $table->timestamps();
        });

        Schema::table('order_link', function (Blueprint $table) {
            $table->index('mw_order_uuid');
            $table->index('mw_order_number');
            $table->index('cdek_order_uuid');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_link', function (Blueprint $table) {
            $table->dropIndex('order_link_mw_order_uuid_index');
            $table->dropIndex('order_link_mw_order_number_index');
            $table->dropIndex('order_link_cdek_order_uuid_index');
            $table->dropIndex('order_link_status_index');
        });

        Schema::dropIfExists('order_link');
    }
}
