<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateErrorAttempt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('error_attempt', function (Blueprint $table) {
            $table->id();
            $table->string('mw_order_uuid');
            $table->string('mw_order_number')->nullable();
            $table->text('cdek_order_request');
            $table->text('cdek_order_response');
            $table->timestamps();
        });

        Schema::table('error_attempt', function (Blueprint $table) {
            $table->index('mw_order_uuid');
            $table->index('mw_order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('error_attempt', function (Blueprint $table) {
            $table->dropIndex('error_attempt_mw_order_uuid_index');
            $table->dropIndex('error_attempt_mw_order_number_index');
        });

        Schema::dropIfExists('error_attempt');
    }
}
