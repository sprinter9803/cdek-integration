<?php

namespace Modules\MyWarehouse\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для обработки данных заказа системы МойСклад
 *
 * @author Oleg Pyatin
 */
class MwOrderData extends BaseDto
{
    public $name;

    public $organization;

    public $attributes;

    public $id;

    public $agent;

    public $positions;
}
