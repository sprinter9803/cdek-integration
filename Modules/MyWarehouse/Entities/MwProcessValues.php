<?php

namespace Modules\MyWarehouse\Entities;

/**
 * Класс для хранения вспомогательной информации для работы с системой МойСклад (URL адресов и др)
 *
 * @author Oleg Pyatin
 */
class MwProcessValues
{
    /**
     * URL для получения данных о заказе
     */
    public const GET_ORDER_INFO_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/';
    /**
     * URL для получения данных о произвольной сущности
     */
    public const GET_ENTITY_INFO_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customentity/';
    /**
     * Модификаторы для получения дополнительной информации
     */
    public const EXTRA_ENTITY_INFO = '?expand=organization,agent,positions.assortment';
    /**
     * Сообщение об ошибке в случае невозможности получить данные о тарифе
     */
    public const ERROR_GET_TARIFF_DATA = 'Ошибка в получении данных тарифа';
}
