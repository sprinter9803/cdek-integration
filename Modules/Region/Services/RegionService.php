<?php

namespace Modules\Region\Services;

use Modules\Cdek\Components\External\CdekConnector;
use Modules\Region\Entities\RegionProcessValues;

/**
 * Сервис для выполнения работы с действиями в аспекте регионов СДЕК-а
 *
 * @author Oleg Pyatin
 */
class RegionService
{
    protected $cdek_connector;

    public function __construct(CdekConnector $cdek_connector)
    {
        $this->cdek_connector = $cdek_connector;
    }

    public function getRegionList()
    {
        return $this->cdek_connector->sendSimpleQuery(RegionProcessValues::CDEK_REGIONS_INFO_GET);
    }

    public function searchCityByName(string $city_name)
    {
        $cdek_response = $this->cdek_connector->sendSimpleQuery(RegionProcessValues::CDEK_CITY_INFO_GET.$city_name);

        if (count($cdek_response)>1) {
            $max_city_index = 0;

            for ($i=1; $i<count($cdek_response); $i++) {
                if (count($cdek_response[$i]["postal_codes"])>count($cdek_response[$i-1]["postal_codes"])) {
                    $max_city_index = $i;
                }
            }

            return $cdek_response[$max_city_index];
        }

        return $cdek_response[0];
    }
}
