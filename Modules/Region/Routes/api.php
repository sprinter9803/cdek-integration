<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'region'], function () {
    Route::get('list', 'RegionController@list')->name('region.list');
    Route::get('city_search', 'RegionController@citySearch')->name('region.city_search');
});
