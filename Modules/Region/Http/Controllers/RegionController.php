<?php

namespace Modules\Region\Http\Controllers;

use Modules\Region\Services\RegionService;
use Modules\Region\Entities\RegionProcessValues;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    protected $region_service;

    public function __construct(RegionService $region_service)
    {
        $this->region_service = $region_service;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function list()
    {
        return $this->region_service->getRegionList();
    }

    public function citySearch(Request $request)
    {
        // Получение имени города из GET-параметра

        return $this->region_service->searchCityByName($request->input(RegionProcessValues::CDEK_CITY_INFO_GET_PARAM_NAME));
    }
}
