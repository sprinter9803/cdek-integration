<?php

namespace Modules\Region\Entities;

/**
 * Класс для хранения вспомогательной информации по работе с регионами
 *
 * @author Oleg Pyatin
 */
class RegionProcessValues
{
    /**
     * URL для получения информации о регионах СДЕК
     */
    public const CDEK_REGIONS_INFO_GET = 'https://api.edu.cdek.ru/v2/location/regions';
    /**
     * URL для получения информации о городах в системе СДЕК (в контексте получения информации о городе)
     */
    public const CDEK_CITY_INFO_GET = 'https://api.cdek.ru/v2/location/cities?city=';
    /**
     * Ключ названия GET-параметра для получения данных о городе в СДЕК
     */
    public const CDEK_CITY_INFO_GET_PARAM_NAME = 'city';
}
