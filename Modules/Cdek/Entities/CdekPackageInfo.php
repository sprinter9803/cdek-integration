<?php

namespace Modules\Cdek\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения данных о содержимом посылки в системе CDEK
 *
 * @author Oleg Pyatin
 */
class CdekPackageInfo extends BaseDto
{
    /**
     * @var string  Номер упаковки - идентификатор в ИС Клиента
     */
    public $number;
    /**
     * @var int  Общий вес в граммах
     */
    public $weight;
    /**
     * @var int  Длина упаковки
     */
    public $length;
    /**
     * @var int  Ширина упаковки
     */
    public $width;
    /**
     * @var int  Высота упаковки
     */
    public $height;
    /**
     * @var array  Список вещей в упаковке
     */
    public $items;
}
