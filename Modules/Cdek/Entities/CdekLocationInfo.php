<?php

namespace Modules\Cdek\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения данных о точках доставки в системе СДЭК
 *
 * @author Oleg Pyatin
 */
class CdekLocationInfo extends BaseDto
{
    /**
     * @var string  Название города
     */
    public $city;
    /**
     * @var string  Строка адреса
     */
    public $address;
    /**
     * @var string  Почтовый код
     */
    public $postal_code;
    /**
     * @var string  Код населенного пункта в системе СДЭК
     */
    public $code;
}
