<?php

namespace Modules\Cdek\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения данных о товаре в посылке системы CDEK
 *
 * @author Oleg Pyatin
 */
class CdekItemInfo extends BaseDto
{
    /**
     * @var string  Идентификатор/артикул товара
     */
    public $ware_key;
    /**
     * @var string  Наименование товара
     */
    public $name;
    /**
     * @var float  Объявленная стоимость товара
     */
    public $cost;
    /**
     * @var Оплата за товар при получении
     */
    public $payment;
    /**
     * @var int  Вес за единицу товара
     */
    public $weight;
    /**
     * @var int  Количество единиц товара (в штуках)
     */
    public $amount;
}
