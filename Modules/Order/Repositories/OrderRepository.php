<?php

namespace Modules\Order\Repositories;

use App\Models\OrderLink;
use App\Models\ErrorAttempt;

/**
 * Класс-репозиторий для работы с БД в аспекте заказов
 *
 * @author Oleg Pyatin
 */
class OrderRepository
{
    /**
     * Функция сохранения новой записи о заказах
     *
     * @param OrderLink $new_order_link  Заполненная запись о создания нового заказа СДЕК
     * @return bool  Сохранилось или нет
     */
    public function saveNewOrderLink(OrderLink $new_order_link)
    {
        return $new_order_link->save();
    }

    /**
     * Функция сохранения записи об ошибках
     *
     * @param ErrorAttempt $new_error_attempt  Заполненная запись о создании ошибочной попытки
     * @return bool  Сохранилось или нет
     */
    public function saveNewErrorAttempt(ErrorAttempt $new_error_attempt)
    {
        return $new_error_attempt->save();
    }

    /**
     * Функция получения заказов которые появились позже чем день назад
     *
     * @return Collection  Список таких заказов, если имеются
     */
    public function getOrdersOlderThanDay()
    {
        return OrderLink::where('created_at', '<', date("Y-m-d H:i:s", time() - (1*24*3600)))->get();
    }

    /**
     * Функция получения заказов которые появились позже чем месяц назад
     *
     * @return Collection  Список таких заказов, если имеются
     */
    public function getOrdersOlderThanMonth()
    {
        return OrderLink::where('created_at', '<', date("Y-m-d H:i:s", time() - (30*24*3600)))->get();
    }

    /**
     * Функция получения заказов по которым требуется проверка статусов
     *
     * @return Collection  Список заказов для проверки, если имеются
     */
    public function getOrdersToCheck($statuses = [])
    {
        return OrderLink::whereNull('status')->orWhereNotIn('status', ["DELIVERED", "NOT_DELIVERED", "INVALID", "CREATED"])->get();
    }

    /**
     * Функция получения заказа по его UUID из МоегоСклада
     *
     * @param string $mw_order_uuid  UUID заказа
     * @return OrderLink  Заказ
     */
    public function getOrderByMwId(string $mw_order_uuid)
    {
        return OrderLink::where('mw_order_uuid', $mw_order_uuid)->first();
    }
}
