<?php

namespace Modules\Order\Services;

use Modules\Cdek\Entities\CdekProcessValues;
use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\Order\Entities\OrderProcessValues;
use Modules\Order\Exceptions\TariffNotSpecifiedException;
use Modules\Order\Exceptions\SellerNotSpecifiedException;
use Modules\Order\Exceptions\RecipientNotFullContactsException;
use Modules\Order\Exceptions\PositionsNotExistException;
use Modules\Order\Exceptions\PvzEmptyWithWarehouseException;
use Modules\Order\Exceptions\RecipientAddressEmptyException;
use Modules\MyWarehouse\Services\MwService;
use Illuminate\Support\Facades\Validator;

/**
 * Сервис для организации логики проверки данных
 *
 * @author Oleg Pyatin
 */
class OrderCheckService
{
    protected $mw_service;

    public function __construct(MwService $mw_service)
    {
        $this->mw_service = $mw_service;
    }

    /**
     * Функция проверки корректности данных заказа МойСКлад
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     */
    public function checkMwOrderData(MwOrderData $mw_order_data)
    {
        $this->checkIsTariffSpecified($mw_order_data);

        $this->checkIsSellerSpecified($mw_order_data);

        $this->checkRecipientContacts($mw_order_data);

        $this->checkPositionsExistance($mw_order_data);

        $this->checkWarehousePvzEmptyCode($mw_order_data);
    }

    public function checkIsTariffSpecified(MwOrderData $mw_order_data)
    {
        if (!isset($mw_order_data->attributes[OrderProcessValues::TARIFF_ATTR_UUID]["name"])) {
            throw new TariffNotSpecifiedException("No info about tariff", 0, null, $mw_order_data->id);
        }
    }

    public function checkIsSellerSpecified(MwOrderData $mw_order_data)
    {
        if (!isset($mw_order_data->attributes[OrderProcessValues::ATTR_SELLER_UUID]["name"])) {
            throw new SellerNotSpecifiedException("No info about seller", 0, null, $mw_order_data->id);
        }
    }

    public function checkRecipientContacts(MwOrderData $mw_order_data)
    {
        $validator = Validator::make($mw_order_data->agent, [
            'phone'=>'required|string|min:6'
        ]);

        if ($validator->fails()) {
            throw new RecipientNotFullContactsException("No full info about recipient", 0, null, $mw_order_data->id);
        }
    }

    public function checkPositionsExistance(MwOrderData $mw_order_data)
    {
        if (count($mw_order_data->positions["rows"]) < 1) {
            throw new PositionsNotExistException("No positions in MW order", 0, null, $mw_order_data->id);
        }
    }

    public function checkWarehousePvzEmptyCode(MwOrderData $mw_order_data)
    {
        $tariff_code = $this->mw_service->getTariffCode($mw_order_data->attributes[OrderProcessValues::TARIFF_ATTR_UUID]["name"],
                                            $mw_order_data->attributes[OrderProcessValues::TARIFF_ATTR_UUID]["meta"]["href"]);

        if (in_array($tariff_code, CdekProcessValues::CDEK_WAREHOUSE_TARIFFES_LIST)) {

            if (!isset($mw_order_data->attributes[OrderProcessValues::ATTR_PVZ_CODE])) {
                throw new PvzEmptyWithWarehouseException("Pvz field empty with warehouse tariff", 0, null, $mw_order_data->id);
            }
        }
    }

    public function checkRecipientAddressFilled(MwOrderData $mw_order_data)
    {
        $tariff_code = $this->mw_service->getTariffCode($mw_order_data->attributes[OrderProcessValues::TARIFF_ATTR_UUID]["name"],
                                            $mw_order_data->attributes[OrderProcessValues::TARIFF_ATTR_UUID]["meta"]["href"]);

        // В случае если тариф подразумевает использование ПВЗ - поле населенного пункта не актуально
        if (in_array($tariff_code, CdekProcessValues::CDEK_WAREHOUSE_TARIFFES_LIST)) {
            return;
        }

        if (!isset($mw_order_data->attributes[OrderProcessValues::ATTR_RECIPIENT_CITY]["meta"]["href"])) {
            throw new RecipientAddressEmptyException("Recipient address is empty", 0, null, $mw_order_data->id);
        }

        $cdek_city_info = $this->mw_service->getCityInfoByHref($mw_order_data->attributes[OrderProcessValues::ATTR_RECIPIENT_CITY]["meta"]["href"]);

        if (strlen(trim($cdek_city_info["name"])) < 1) {
            throw new RecipientAddressEmptyException("Recipient address is empty", 0, null, $mw_order_data->id);
        }
    }
}
