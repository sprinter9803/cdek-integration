<?php

namespace Modules\Order\Services;

use Modules\Cdek\Components\External\CdekConnector;
use Modules\Cdek\Entities\CdekPackageInfo;
use Modules\Cdek\Entities\CdekItemInfo;
use Modules\Cdek\Entities\CdekLocationInfo;
use Modules\Order\Factories\OrderFactory;
use Modules\Order\Repositories\OrderRepository;
use Modules\Order\Entities\DadataAddressFields;
use Modules\Order\Entities\OrderProcessValues;
use Modules\Order\Entities\OrderCreateData;
use Modules\Order\Entities\OrderDeliveryAddress;
use Modules\Order\Entities\OrderFormingParams;
use Modules\Order\Services\OrderReportService;
use Modules\Order\Services\OrderCheckService;
use Modules\Order\Exceptions\ErrorCdekOrderCreateException;
use Modules\Order\Components\DadataConnector;
use Modules\Order\Jobs\ProcessOrder;
use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\MyWarehouse\Services\MwService;
use App\Components\ArrayHelper;

/**
 * Сервис для выполнения работы с заказами
 *
 * @author Oleg Pyatin
 */
class OrderService
{
    protected $cdek_connector;

    protected $order_factory;

    protected $order_repository;

    protected $dadata;

    public function __construct(CdekConnector $cdek_connector, MwService $mw_service, OrderReportService $order_report_service,
                                    OrderCheckService $order_check_service, OrderFactory $order_factory, OrderRepository $order_repository, DadataConnector $dadata)
    {
        $this->cdek_connector = $cdek_connector;
        $this->mw_service = $mw_service;
        $this->order_report_service = $order_report_service;
        $this->order_check_service = $order_check_service;

        $this->order_factory = $order_factory;
        $this->order_repository = $order_repository;
        $this->dadata = $dadata;
    }

    /**
     * Основная функция создания заказа в системе СДЭК
     *
     * @param MwOrderData $mw_order_data  Данные о заказе МС
     * @return string  UUID сформированного заказа
     */
    public function createOrder(MwOrderData $mw_order_data)
    {
        $this->order_check_service->checkMwOrderData($mw_order_data);

        $order_create_data = OrderCreateData::loadFromArray([
            'recipient'=>[
                'name'=>$mw_order_data->agent["name"],
                'phones'=>[
                    [
                        // Номер телефона в аттрибутах заказа приоритетней номера из карточки контрагента
                        "number"=> \Arr::get($mw_order_data->attributes, OrderProcessValues::ATTR_RECIP_PHONE, $mw_order_data->agent["phone"])
                    ]
                ],
                // Почта покупателя в аттрибутах заказа приоритетней почты из карточки контрагента
                'email'=>\Arr::get($mw_order_data->attributes, OrderProcessValues::ATTR_RECIP_EMAIL, $mw_order_data->agent["email"] ?? '')
            ],
            'sender'=>[
                'name'=>$mw_order_data->organization["name"]
            ],

//          'number'=>$this->getTestingDevelopUniqueOrderNumber(),//$mw_order_data->name, // Временный вариант чтобы СДЕК не генерировал ошибки

            'number'=>$mw_order_data->name, // Основной вариант, переставить на него когда будет итоговое включение в систему (и будет делаться
//               на каждый новый заказ МС один заказа СДЕК-а)

            'tariff_code'=>$this->mw_service->getTariffCode($mw_order_data->attributes[OrderProcessValues::TARIFF_ATTR_UUID]["name"],
                                            $mw_order_data->attributes[OrderProcessValues::TARIFF_ATTR_UUID]["meta"]["href"]),

            'seller'=>[
                'name'=> $mw_order_data->attributes[OrderProcessValues::ATTR_SELLER_UUID]["name"] ??
                            $mw_order_data->attributes[OrderProcessValues::ATTR_SELLER_UUID]
            ],
            'comment'=>$mw_order_data->attributes[OrderProcessValues::ATTR_COMMENT_UUID] ?? OrderProcessValues::DEFAULT_PACKAGE_COMMENT,

            'packages'=>$this->processPackageData($mw_order_data),

            'delivery_recipient_cost'=>$this->getDeliveryRecipientCost($mw_order_data)
        ]);


        if ($this->checkAddressTypePvz($mw_order_data)) {
            // В случае ПВЗ нам не нужно взятие из таблицы кода города в новой версии API

            // Код для использования с боевой учеткой (где список ПВЗ в 3 раза больше)
            $order_create_data->delivery_point = $mw_order_data->attributes[OrderProcessValues::ATTR_PVZ_CODE];

            // Временно используем тестовый ПВЗ работая на тестовой учетной записи
            //$order_create_data->delivery_point = OrderProcessValues::TEST_PVZ_CODE;
            $order_create_data->from_location = $this->getSenderAddressData($mw_order_data);

        } else {
            $order_create_data->to_location = $this->getRecipientAddressData($mw_order_data);
            $order_create_data->from_location = $this->getSenderAddressData($mw_order_data);  // ! Основная функция но ему не достает почтового индекса поэтому пока ставим тестовую
        }

        $order_create_data->services = $this->getOrderExtraServices($mw_order_data);

        $cdek_order_result = $this->cdek_connector->sendDataQuery(OrderProcessValues::CDEK_ORDER_CREATE_URL, ArrayHelper::toArray($order_create_data));


        if (isset($cdek_order_result["entity"]["uuid"])) {

            $new_cdek_order_uuid = $cdek_order_result["entity"]["uuid"];

            $new_order_link = $this->order_factory->createOrderLink(OrderFormingParams::loadFromArray([
                'mw_order_uuid'=>$mw_order_data->id,
                'mw_order_number'=>$order_create_data->number,
                'cdek_order_uuid'=>$new_cdek_order_uuid,
                'cdek_order_request'=>json_encode($order_create_data),
                'cdek_order_response'=>json_encode($cdek_order_result),
            ]));

            $this->order_repository->saveNewOrderLink($new_order_link);

            ProcessOrder::dispatch($mw_order_data->id, $new_cdek_order_uuid)
                        ->delay(now()->addSeconds(10));

            return $new_cdek_order_uuid;

        } else {

            $new_error_attempt = $this->order_factory->createErrorAttempt(OrderFormingParams::loadFromArray([
                'mw_order_uuid'=>$mw_order_data->id,
                'mw_order_number'=>$order_create_data->number,
                'cdek_order_request'=>json_encode($order_create_data),
                'cdek_order_response'=>json_encode($cdek_order_result),
            ]));

            $this->order_repository->saveNewErrorAttempt($new_error_attempt);

            throw new ErrorCdekOrderCreateException("Error on Cdek Order creating step", 0, null, $mw_order_data->id);
        }
    }


    /**
     * Функция разбор содержимого посылок
     *
     * @param MwOrderData $mw_order_data
     * @return type
     */
    public function processPackageData(MwOrderData $mw_order_data)
    {
        $package_number = 1;
        $all_packages = [];

        foreach ($mw_order_data->positions["rows"] as $package) {

            // В случае указывания доставки ее пропускаем - она обрабатывается отдельно
            if ($package["assortment"]["code"] === OrderProcessValues::DELIVERY_COST_CODE) {
                continue;
            }

            // Добавляем только товары, без услуг
            if ($package["assortment"]["paymentItemType"] === 'SERVICE') {
                continue;
            }

            $assortment_attributes = $this->mw_service->prepareAssortmentAttrs($package["assortment"]["attributes"] ?? []);

            $cdek_package = CdekPackageInfo::loadFromArray([
                'number'=>$package_number++,
                'length'=>$assortment_attributes[OrderProcessValues::ATTR_ASSORT_LENGTH] ?? 0,
                'width'=>$assortment_attributes[OrderProcessValues::ATTR_ASSORT_WIDTH] ?? 0,
                'height'=>$assortment_attributes[OrderProcessValues::ATTR_ASSORT_HEIGHT] ?? 0,
            ]);


            // Обработка цены и стоимости
            $is_order_payed = (bool)($mw_order_data->attributes[OrderProcessValues::ATTR_ORDER_IS_PAYED] ?? false);

            $price = $package["price"] / 100;

            $price_discount = $price;
            if (($discount = (int)$package["discount"]) > 0) {
                $price_discount = $price * ((100 - $discount)/100);
            }


            // Обработка имени и артикула
            $item_name = $package["id"];

            if (isset($package["assortment"]["name"])) {
                $item_name = preg_replace( '/"([^"]*)"/', "«$1»", $package["assortment"]["name"]);
            }

            $ware_key = $item_name;
            if ($package["assortment"]["code"]) {
                $ware_key = $package["assortment"]["code"];
            }

            if (strlen($ware_key) > OrderProcessValues::WARE_KEY_LENGTH_LIMIT) {
                $ware_key = mb_substr($ware_key, 0, OrderProcessValues::WARE_KEY_LENGTH_LIMIT, 'utf-8');
            }

            // Обработка веса
            $weight = 0;
            if ($package["assortment"]["meta"]["type"]!=="service") {

                $weight = OrderProcessValues::PACKAGE_DEFAULT_WEIGHT;
                if (isset($package["assortment"]["weight"])) {
                    $weight = (int)($package["assortment"]["weight"] * 1000);
                }
            }

            $cdek_item = CdekItemInfo::loadFromArray([
                'ware_key'=>$ware_key,
                'name'=>$item_name,
                'weight'=>$weight,
                'amount'=>$package["quantity"],
                'cost'=>$price,
                'payment'=>[
                    'value'=>($is_order_payed) ? 0 : $price_discount,
                    'vat_rate'=>OrderProcessValues::VAT_DEFAULT_VALUE,
                    'vat_sum'=>0.0,
                ]
            ]);

            // Настройка НДС
            $package_vat = $package["assortment"]["vat"] ?? 0;

            if ($package_vat>0) {
                $cdek_item->payment["vat_rate"] = $package_vat;
                $cdek_item->payment["vat_sum"] = ($cdek_item->payment["value"]/100 - $cdek_item->payment["value"] / (100 + $package_vat)) * 100;
            }

            $cdek_package->weight = $weight;

            $cdek_package->items = [
                ArrayHelper::toArray($cdek_item)
            ];

            $all_packages[] = ArrayHelper::toArray($cdek_package);
        }

        return $all_packages;
    }

    /**
     * Получение данных адреса отправителя (в текущем варианте достаточно почтового кода Москва 76)
     * @param MwOrderData $mw_order_data  Данные о заказе МС
     * @return type
     */
    public function getSenderAddressData(MwOrderData $mw_order_data)
    {
        return CdekLocationInfo::loadFromArray([
            'code'=>OrderProcessValues::SENDER_MOSCOW_REGION_CDEK_CODE,
            'postal_code'=> OrderProcessValues::SEND_CITY_DEFAULT_POST_CODE,
            'address'=>OrderProcessValues::SENDER_ADDRESS_STRING,
        ]);
    }

    /**
     * Получение данных адреса доставки (общие и сам адрес и код города и пр)
     * @param MwOrderData $mw_order_data  Данные о заказе МС
     * @return type
     */
    public function getRecipientAddressData(MwOrderData $mw_order_data)
    {
        if (isset($mw_order_data->attributes[OrderProcessValues::ATTR_RECIPIENT_MW_ORDER_ADDR])) {
            $dadata_info = $this->tryDadataForAddressProcessing($mw_order_data, $mw_order_data->attributes[OrderProcessValues::ATTR_RECIPIENT_MW_ORDER_ADDR]);
        }

        // Проверяем наличие поля населенный пункт, если его нет - пробуем заполнять данные из Dadata
        if (isset($mw_order_data->attributes[OrderProcessValues::ATTR_RECIPIENT_CITY])) {

            $cdek_city_info = $this->mw_service->getCityInfoByHref($mw_order_data->attributes[OrderProcessValues::ATTR_RECIPIENT_CITY]["meta"]["href"]);

            $city_name = explode(',', $cdek_city_info["name"])[0];
            $prepared_city_name = trim(str_replace('г', '', $city_name));

            return CdekLocationInfo::loadFromArray([
                'city'=>$prepared_city_name,
                'address'=>$this->getAddressString($mw_order_data, $dadata_info),
                'code'=>$cdek_city_info["code"] ?? 0,
            ]);

        } else {

            // Дополнительное получение СДЕК кода города от Dadata
            $transport_company_data = $this->dadata->getTransportCompanyCode($dadata_info->city_kladr_id);

            return CdekLocationInfo::loadFromArray([
                'city'=>$dadata_info->city ?? '',
                'address'=>$this->getAddressString($mw_order_data, $dadata_info),
                'code'=>$transport_company_data["suggestions"][0]["data"]["cdek_id"] ?? 0,
            ]);
        }
    }


    /**
     * Получение строки адреса (улица, квартира, дом), с подстраховкой данных от сервиса Dadata
     * @param MwOrderData $mw_order_data  Данные о заказе МС
     * @return type
     */
    public function getAddressString(MwOrderData $mw_order_data, DadataAddressFields $dadata_info)
    {
        $address = OrderDeliveryAddress::loadFromArray([
            'index'=>$mw_order_data->attributes[OrderProcessValues::ATTR_RECIP_ADDR_INDEX] ?? $dadata_info->postal_code ?? '',
            'street'=>$mw_order_data->attributes[OrderProcessValues::ATTR_RECIP_ADDR_STREET] ?? $dadata_info->street ?? '',
            'house'=>$mw_order_data->attributes[OrderProcessValues::ATTR_RECIP_ADDR_HOUSE] ?? $dadata_info->house ?? '',
            'structure'=>$mw_order_data->attributes[OrderProcessValues::ATTR_RECIP_ADDR_STRUCTURE] ?? $dadata_info->block ?? '',
            'flat'=>$mw_order_data->attributes[OrderProcessValues::ATTR_RECIP_ADDR_FLAT] ?? $dadata_info->flat ?? ''
        ]);

        $final_address = '';

        $final_address .= (strlen($address->index)>0) ? $address->index : '';
        $final_address .= (strlen($address->street)>0) ? ', '.$address->street : '';
        $final_address .= (strlen($address->house)>0) ? ', д.'.$address->house : '';
        $final_address .= (strlen($address->structure)>0) ? ', стр./к. '.$address->structure : '';
        $final_address .= (strlen($address->flat)>0) ? ', кв.'.$address->flat : '';

        return $final_address;
    }

    /**
     * Проверяем заказ с доставкой через ПВЗ или курьерного типа
     * @param MwOrderData $mw_order_data  Данные о заказе МС
     * @return bool
     */
    public function checkAddressTypePvz(MwOrderData $mw_order_data)
    {
        if (isset($mw_order_data->attributes[OrderProcessValues::ATTR_PVZ_CODE])) {
            return true;
        }
        return false;
    }

    /**
     * Получение списка дополнительных услуг/сервисов, указанных в МС заказе
     *
     * @param MwOrderData $mw_order_data  Данные о заказе МС
     * @return array  Список услуг подготовленных к заполнению в СДЭК
     */
    public function getOrderExtraServices(MwOrderData $mw_order_data)
    {
        $services = [];

        if (isset($mw_order_data->attributes[OrderProcessValues::ATTR_SERVICE_SAFE_DELIVERY])) {
            $services[] = [
                'code'=>OrderProcessValues::SERVICE_SAFE_DELIVERY_MARK
            ];
        }

        if (isset($mw_order_data->attributes[OrderProcessValues::ATTR_SERVICE_INSPECT_DELIVERY])) {
            $services[] = [
                'code'=>OrderProcessValues::SERVICE_INSPECT_DELIVERY_MARK
            ];
        }

        return $services;
    }

    public function getDeliveryRecipientCost(MwOrderData $mw_order_data)
    {
        foreach ($mw_order_data->positions["rows"] as $package) {
            // В случае указывания доставки ее пропускаем - она обрабатывается отдельно
            if ($package["assortment"]["code"] === OrderProcessValues::DELIVERY_COST_CODE) {

                $is_order_payed = (bool)($mw_order_data->attributes[OrderProcessValues::ATTR_ORDER_IS_PAYED] ?? false);
                $price = $package["price"] / 100;

                $price_discount = $price;
                if (($discount = (int)$package["discount"]) > 0) {
                    $price_discount = $price * ((100 - $discount)/100);
                }

                $delivery_cost_value = ($is_order_payed) ? 0 : $price_discount;

                $package_vat = $package["assortment"]["vat"] ?? 0;
                if ($package_vat>0) {
                    $vat_rate = $package_vat;
                    $vat_sum = ($delivery_cost_value/100 - $delivery_cost_value / (100 + $package_vat)) * 100;
                }

                return [
                    'value'=>$delivery_cost_value,
                    'vat_sum'=>$vat_sum,
                    'vat_rate'=>$vat_rate
                ];
            }
        }

        // Случай по-умолчанию - если ничего не нашлось считаем что за доставку не платим
        return [
            'value'=>0,
        ];
    }

    /**
     * Функция получения информации о заказе в системе CDEK по его UUID
     * @param string $cdek_order_uuid  UUID заказа в системе CDEK
     * @return array
     */
    public function getCdekOrderInfo(string $cdek_order_uuid)
    {
        return $this->cdek_connector->sendSimpleQuery(OrderProcessValues::CDEK_ORDER_GET_URL.$cdek_order_uuid);
    }

    /**
     * Функция получения данных списка ПВЗ
     * @return array
     */
    public function getPvzData()
    {
        return $this->cdek_connector->sendSimpleQuery(OrderProcessValues::CDEK_DELIVERY_POINT_LIST);
    }

    /**
     * Функция для получения нового тестового имени заказа (СДЕК генерирует ошибки если мы пытаемся оформить по второму
     *     разу уже оформленный заказ из какой-то сторонней системы)
     *
     * // Переключить к обычному варианту при переводе на боевой
     */
    public function getTestingDevelopUniqueOrderNumber()
    {
        return "SPEC-890-".mt_rand(1,100000);
    }

    /**
     * Функция запуска сервиса Dadata - пробуем улучшить знания об адресе покупателя если для этого имеется возможность
     * @param string $mw_order_uuid  UUID заказа в системе МойСклад (нужен для обратной доработки информации)
     * @param string $address  Текущий имеющийся адрес покупателя
     * @return void  Ничего не возвращаем
     */
    public function tryDadataForAddressProcessing(MwOrderData $mw_order_data, string $address)
    {
        $dadata_address = \Arr::get($this->dadata->getCleanAddress($address), '0', []);

        $dadata_info = DadataAddressFields::loadFromArray($dadata_address);

        $full_address = $dadata_info->postal_code . ', ' . $dadata_info->region_with_type . ', ' . $dadata_info->city_with_type . ', ' .$dadata_info->street_with_type;

        $full_address .= ', ' . $dadata_info->house_type_full . ' ' . $dadata_info->house;

        if (!empty($dadata_info->block)) {
            $full_address .= ', ' . $dadata_info->block_type_full . ' ' . $dadata_info->block;
        }

        $full_address .= ', ' . $dadata_info->flat_type . ' ' . $dadata_info->flat;

        $address_attrs = $this->order_report_service->getDetailedAddress($mw_order_data, $dadata_info);

        $this->order_report_service->sendDadataInfo($mw_order_data->id, "Адрес от системы Dadata: ".$full_address, $address_attrs);

        // В случае если в Dadata нету данных о городе - делаем проверку имеется ли о нем уже занесенная запись менеджером в заказе,
        //     Если нет - выходит ошибка
        if (empty($dadata_info->region_with_type) && empty($dadata_info->city_with_type)) {
            $this->order_check_service->checkRecipientAddressFilled($mw_order_data);
        }

        return $dadata_info;
    }
}
