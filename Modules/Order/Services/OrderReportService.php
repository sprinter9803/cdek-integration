<?php

namespace Modules\Order\Services;

use Modules\Order\Entities\OrderProcessValues;
use Modules\Order\Entities\OrderDeliveryNote;
use Modules\Order\Entities\OrderBarCode;
use Modules\Cdek\Components\External\CdekConnector;
use Modules\MyWarehouse\Components\MyWarehouseConnector;
use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\Order\Entities\DadataAddressFields;
use Modules\Order\Jobs\SaveBarCode;
use Modules\Order\Jobs\SaveDeliveryNote;
use App\Components\ArrayHelper;
use Illuminate\Support\Facades\Storage;

/**
 * Сервис для выполнения действий после успешного создания заказа в СДЕК
 *
 * @author Oleg Pyatin
 */
class OrderReportService
{
    protected $cdek_connector;

    protected $mw_connector;

    public function __construct(CdekConnector $cdek_connector, MyWarehouseConnector $mw_connector)
    {
        $this->cdek_connector = $cdek_connector;
        $this->mw_connector = $mw_connector;
    }

    public function doFinalActions(string $base_mw_order_number, string $new_cdek_order_uuid, string $cdek_order_number)
    {
        $this->formDeliveryNote($new_cdek_order_uuid, $cdek_order_number);

        $this->formBarCode($new_cdek_order_uuid, $cdek_order_number);

        $this->saveReportToMw($base_mw_order_number, $cdek_order_number);

        $this->sendDeliverySum($base_mw_order_number, $new_cdek_order_uuid);
    }

    /**
     * Оформление накладной к заказу
     * @param string $new_cdek_order_uuid  UUID нового созданного заказа
     * @param string $cdek_order_number  Номер заказа Сдэк
     * @return void
     */
    public function formDeliveryNote(string $new_cdek_order_uuid, string $cdek_order_number)
    {
        $create_delivery_note = OrderDeliveryNote::loadFromArray([
            'orders'=>[
                [
                    'order_uuid'=>$new_cdek_order_uuid
                ]
            ],
            'copy_count'=> OrderProcessValues::DELIVERY_NOTE_COPY_COUNT,
        ]);

        $cdek_delivery_note = $this->cdek_connector->sendDataQuery(OrderProcessValues::CDEK_DELIVERY_NOTE_CREATE, ArrayHelper::toArray($create_delivery_note));
        $cdek_delivery_note_uuid = trim($cdek_delivery_note["entity"]["uuid"]);

        SaveDeliveryNote::dispatch($cdek_delivery_note_uuid, $cdek_order_number)
                        ->delay(now()->addSeconds(35));
    }

    public function saveDeliveryNote(string $cdek_delivery_note_uuid, string $cdek_order_number)
    {
        $pdf = $this->cdek_connector->processDataQuery(OrderProcessValues::CDEK_DELIVERY_NOTE_FILE_GET.$cdek_delivery_note_uuid.".pdf");
        Storage::disk(OrderProcessValues::SAVE_CDEK_FILES_DISK)->put($cdek_order_number.'.pdf', $pdf);
    }

    /**
     * Оформление наклейки к заказу
     * @param string $new_cdek_order_uuid  UUID нового созданного заказа
     * @param string $cdek_order_number  Номер заказа Сдэк
     * @return void
     */
    public function formBarCode(string $new_cdek_order_uuid, string $cdek_order_number)
    {
        $create_bar_code = OrderBarCode::loadFromArray([
            'orders'=>[
                [
                    'order_uuid'=>$new_cdek_order_uuid
                ]
            ],
            'copy_count'=> OrderProcessValues::DELIVERY_BAR_CODE_COUNT,
            'format'=>OrderProcessValues::DELIVERY_BAR_CODE_FORMAT,
            'lang'=>OrderProcessValues::DELIVERY_BAR_CODE_LANG
        ]);

        $cdek_delivery_bar_code = $this->cdek_connector->sendDataQuery(OrderProcessValues::CDEK_BAR_CODE_CREATE, ArrayHelper::toArray($create_bar_code));
        $cdek_bar_code_uuid = trim($cdek_delivery_bar_code["entity"]["uuid"]);

        SaveBarCode::dispatch($cdek_bar_code_uuid, $cdek_order_number)
                     ->delay(now()->addSeconds(25));

    }

    public function saveBarCode(string $cdek_bar_code_uuid, string $cdek_order_number)
    {
        $pdf = $this->cdek_connector->processDataQuery(OrderProcessValues::CDEK_BAR_CODE_GET.$cdek_bar_code_uuid.".pdf");
        Storage::disk(OrderProcessValues::SAVE_CDEK_FILES_DISK)->put($cdek_order_number."_pack.pdf", $pdf);
    }

    public function saveReportToMw(string $base_mw_order_number, string $cdek_order_number)
    {
        $this->mw_connector->updateDataQuery(OrderProcessValues::MW_ORDER_CHANGE_URL.$base_mw_order_number, [
            'attributes'=>[
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_CDEK_ORDER_NUMBER,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>$cdek_order_number
                ],
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_CDEK_DELIVERY_NOTE,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>Storage::disk(OrderProcessValues::SAVE_CDEK_FILES_DISK)->url($cdek_order_number.".pdf")
                ],
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_CDEK_BARCODE,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>Storage::disk(OrderProcessValues::SAVE_CDEK_FILES_DISK)->url($cdek_order_number."_pack.pdf")
                ],
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_ERROR_MESSAGE,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>''
                ],
            ],

            'state'=>[
                'meta'=>[
                    'href'=>OrderProcessValues::MW_ORDER_CHANGE_STATE_URL . OrderProcessValues::MW_ATTR_CDEK_ORDER_SUCCESS,
                    'type'=>'state',
                    'mediaType'=>'application/json'
                ]
            ]
        ]);
    }


    public function sendDeliverySum(string $base_mw_order_number, string $new_cdek_order_uuid)
    {
        $cdek_order_info = $this->cdek_connector->sendSimpleQuery(OrderProcessValues::CDEK_ORDER_GET_URL.$new_cdek_order_uuid);

        $final_sum = 0.0;
        $services = $cdek_order_info["entity"]["services"] ?? [];

        foreach ($services as $cdek_order_service) {
            $final_sum += (float)$cdek_order_service["sum"];
        }

        $final_sum += (float)($cdek_order_info["entity"]["delivery_detail"]["delivery_sum"] ?? 0);

        $this->mw_connector->updateDataQuery(OrderProcessValues::MW_ORDER_CHANGE_URL.$base_mw_order_number, [
            'attributes'=>[
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_DELIVERY_COST,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>$final_sum
                ],
            ],
        ]);
    }

    /**
     * Функция для отсылки комментария к заказу МойСклад (Одно из применений - использование ответа от Dadata)
     *
     * @param string $base_mw_order_number  Номер заказа МойСклад
     * @param string $comment  Текст нового комментария
     * @return void  Пока Ничего не возвращаем
     */
    public function sendDadataInfo(string $base_mw_order_number, string $comment, array $detail_address_attrs)
    {
        // Записываем адрес от Дадаты в лог, но не передаём его в МС,
        //   потому что в поле "Комментарий" могут быть комментарии менеджеров,
        //   которые нежелательно перезаписывать
        \Log::debug("sendDadataInfo: $comment");

        $response = $this->mw_connector->updateDataQuery(OrderProcessValues::MW_ORDER_CHANGE_URL.$base_mw_order_number, [
            'attributes'=>$detail_address_attrs
        ]);
    }


    public function getDetailedAddress(MwOrderData $mw_order_data, DadataAddressFields $dadata_info)
    {
        $updated_attributes = [];

        // Поле населенного пункта контролируется дополнительно в вывзывающей функции - оно должно быть заполнено иначе
        //     выбрасывается ошибка, но этот параметр имеет сложный тип поэтому требует специального заполнения
//        if (!isset($mw_order_data->attributes[OrderProcessValues::ATTR_RECIPIENT_CITY]) && !empty($dadata_info->region_with_type) && !empty($dadata_info->city_with_type)) {
//            $updated_attributes[] = $this->checkAndFillAttribute($dadata_info->region_with_type . ' ' . $dadata_info->city_with_type, OrderProcessValues::ATTR_RECIPIENT_CITY);
//        }

        if (!isset($mw_order_data->attributes[OrderProcessValues::ATTR_RECIP_ADDR_INDEX]) && !empty($dadata_info->postal_code)) {
            $updated_attributes[] = $this->checkAndFillAttribute($dadata_info->postal_code, OrderProcessValues::ATTR_RECIP_ADDR_INDEX);
        }

        if (!isset($mw_order_data->attributes[OrderProcessValues::ATTR_RECIP_ADDR_STREET]) && !empty($dadata_info->street)) {
            $updated_attributes[] = $this->checkAndFillAttribute($dadata_info->street, OrderProcessValues::ATTR_RECIP_ADDR_STREET);
        }

        if (!isset($mw_order_data->attributes[OrderProcessValues::ATTR_RECIP_ADDR_HOUSE]) && !empty($dadata_info->house)) {
            $updated_attributes[] = $this->checkAndFillAttribute($dadata_info->house, OrderProcessValues::ATTR_RECIP_ADDR_HOUSE);
        }

        if (!isset($mw_order_data->attributes[OrderProcessValues::ATTR_RECIP_ADDR_STRUCTURE]) && !empty($dadata_info->block)) {
            $updated_attributes[] = $this->checkAndFillAttribute($dadata_info->block, OrderProcessValues::ATTR_RECIP_ADDR_STRUCTURE);
        }

        if (!isset($mw_order_data->attributes[OrderProcessValues::ATTR_RECIP_ADDR_FLAT]) && !empty($dadata_info->flat)) {
            $updated_attributes[] = $this->checkAndFillAttribute($dadata_info->flat, OrderProcessValues::ATTR_RECIP_ADDR_FLAT);
        }

        if (!isset($mw_order_data->attributes[OrderProcessValues::ATTR_RECIP_ADDR_CITY]) && !empty($dadata_info->city)) {
            $updated_attributes[] = $this->checkAndFillAttribute($dadata_info->city, OrderProcessValues::ATTR_RECIP_ADDR_CITY);
        }

        return $updated_attributes;
    }

    /**
     * Функция для правки атрибута в запросе изменения заказа МС
     * @param string $dadata_param  Параметр от системы Dadata (Хотя можно ставить любой)
     * @param string $attr_uuid  UUID атрибута
     * @return array  Массив который заносится в список изменяемых атрибутов
     */
    public function checkAndFillAttribute(string $dadata_param, string $attr_uuid)
    {
        return [
            'meta'=>[
                'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . $attr_uuid,
                'type'=>'attributemetadata',
                'mediaType'=>'application/json',
            ],
            'value'=>(string)$dadata_param
        ];
    }
}
