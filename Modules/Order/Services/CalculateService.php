<?php

namespace Modules\Order\Services;

use Modules\Order\Entities\CalculateProcessValues;
use Modules\Cdek\Components\External\CdekConnector;
use Modules\Order\Entities\CalculateQueryData;
use Modules\Order\Entities\CalculateQueryResponse;
use App\Components\ArrayHelper;

/**
 * Сервис для организации логики периодической проверки состояния заказов
 *
 * @author Oleg Pyatin
 */
class CalculateService
{
    protected $cdek_connector;

    public function __construct(CdekConnector $cdek_connector)
    {
        $this->cdek_connector = $cdek_connector;
    }

    public function calculateDeliveryCostOnTariff(array $request_params)
    {
        $calculate_delivery_cost_data = CalculateQueryData::loadFromArray($request_params);

        $cdek_order_result = $this->cdek_connector->sendDataQuery(CalculateProcessValues::CDEK_DELIVERY_COST_CALCULATE_URL, ArrayHelper::toArray($calculate_delivery_cost_data));

        $cdek_api_response = CalculateQueryResponse::loadFromArray($cdek_order_result);

        return ArrayHelper::toArray($cdek_api_response);
    }
}
