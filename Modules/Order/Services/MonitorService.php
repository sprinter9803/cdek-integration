<?php

namespace Modules\Order\Services;

use Modules\Order\Entities\OrderProcessValues;
use Modules\MyWarehouse\Components\MyWarehouseConnector;
use Modules\Order\Repositories\OrderRepository;
use Modules\Order\Services\OrderService;
use Modules\Order\Entities\MonitorProcessValues;
use Modules\MyWarehouse\Entities\MwProcessValues;

/**
 * Сервис для организации логики периодической проверки состояния заказов
 *
 * @author Oleg Pyatin
 */
class MonitorService
{
    protected $order_repository;

    protected $order_service;

    protected $mw_connector;

    protected $mw_delivery_statuses = false;

    public function __construct(OrderRepository $order_repository, OrderService $order_service, MyWarehouseConnector $mw_connector)
    {
        $this->order_repository = $order_repository;
        $this->order_service = $order_service;
        $this->mw_connector = $mw_connector;
    }

    /**
     * Выполняем мониторинг изменения стоимости доставки для заказов
     * @return string  Если все хорошо выводим сообщение об успешности
     */
    public function checkOrderDeliveryCostStates()
    {
        $actual_orders = $this->order_repository->getOrdersOlderThanDay();

        foreach ($actual_orders as $order) {

            $cdek_order_data = $this->order_service->getCdekOrderInfo($order->cdek_order_uuid);

            $actual_cost = $this->calculateActualDeliveryCost($cdek_order_data);

            $this->changeMwOrderDeliveryCostToActual($order->mw_order_uuid, $actual_cost);
        }

        return MonitorProcessValues::DELIVERY_COST_SUCCESS_UPDATE;
    }

    /**
     * Расчет актуальной стоимости доставки для заказа СДЕК
     * @param array $cdek_order_info  Информация о заказе СДЕК
     * @return float  Итоговая сумма
     */
    public function calculateActualDeliveryCost(array $cdek_order_info)
    {
        $final_sum = 0.0;
        $services = $cdek_order_info["entity"]["services"] ?? [];

        foreach ($services as $cdek_order_service) {
            $final_sum += (float)$cdek_order_service["sum"];
        }

        $final_sum += (float)($cdek_order_info["entity"]["delivery_detail"]["delivery_sum"] ?? 0);

        return $final_sum;
    }

    /**
     * Функция обновления стоимости доставки в заказе МС
     *
     * @param string $mw_order_uuid  UUID заказа в МС
     * @param string $actual_cost  Актуальная стоимость доставки
     * @return void Ничего не возвращаем просто обновляем
     */
    public function changeMwOrderDeliveryCostToActual(string $mw_order_uuid, string $actual_cost)
    {
        $this->mw_connector->updateDataQuery(OrderProcessValues::MW_ORDER_CHANGE_URL.$mw_order_uuid, [
            'attributes'=>[
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_DELIVERY_COST,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>$actual_cost
                ],
            ],
        ]);
    }

    /**
     * Выполняем мониторинг статусов заказов СДЕК-а
     * @return string  Если все хорошо выводим сообщение об успешности
     */
    public function checkOrderStatuses()
    {
        $orders = $this->order_repository->getOrdersToCheck()->take(25)->mapWithKeys(function ($order) {
            return [
                $order->mw_order_number => $this->checkOrderStatus($order)
            ];
        });

        return response()->json([
            'orders' => $orders->all()
        ]);
    }

    /**
     * Выполняем проверку статуса отдельного заказа СДЕК-а
     * @param OrderLink $order  Заказ
     * @return string  Выводим сообщение об успешности либо текст ошибки
     */
    public function checkOrderStatus($order)
    {

        $cdek_order_data = $this->order_service->getCdekOrderInfo($order->cdek_order_uuid);
        $cdek_status_history = \Arr::get($cdek_order_data, "entity.statuses");
        $cdek_status = collect($cdek_status_history)->map(function ($status) {
            return [
                'code' => $status['code'],
                'meta' => $this->getMwDeliveryStatus($status['code']),
                'date' => $status['date_time']
            ];
        })
        ->filter(function ($status) {
            return ($status['meta'] != false || $status['code'] == 'ACCEPTED' || $status['code'] == 'CREATED' || $status['code'] == 'INVALID');
        })
        ->first();

        if (!$cdek_status) {
            return 'Статус СДЭК -- ?';
        }

        $order->status = $cdek_status['code'];
        $order->save();

        if ($cdek_status['code'] == 'CREATED' || $cdek_status['code'] == 'INVALID') {
            return "Статус СДЭК {$cdek_status['code']} -- пропуск";
        }


        $mw_order_data = $this->mw_connector->sendSimpleQuery(MwProcessValues::GET_ORDER_INFO_URL.
                        $order->mw_order_uuid.MwProcessValues::EXTRA_ENTITY_INFO);

        $mw_status_uuid = substr($mw_order_data["state"]["meta"]["href"], -36);

        $mw_update_attributes = [];

		// Если для текущего статуса СДЭК есть соответствующий аналог среди статусов ТК в МС
		// передаём этот статус и дату его изменения
        if ($mw_delivery_status = $this->getMwDeliveryStatus($cdek_status['code'])) {

            $mw_update_attributes[] = [
                'meta'=>[
                    'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_DELIVERY_COMPANY_STATE,
                    'type'=>'attributemetadata',
                    'mediaType'=>'application/json',
                ],
                'value'=>$mw_delivery_status
            ];

            $mw_update_attributes[] = [
                'meta'=>[
                    'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_DELIVERY_COMPANY_STATE_CHANGE_DATE,
                    'type'=>'attributemetadata',
                    'mediaType'=>'application/json',
                ],
                'value'=>date('Y-m-d H:i:s', strtotime($cdek_status['date']))
            ];

        }


        // Если в данных от СДЭКа есть себестоимость, передаём её
        if ($cdek_order_cost = \Arr::get($cdek_order_data, 'entity.delivery_detail.total_sum', 0)) {
            if (intval($cdek_order_cost) > 0) {
                $mw_update_attributes[] = [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_DELIVERY_COST,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>intval($cdek_order_cost)
                ];
            }
        }

        $mw_update_data = [];
        $mw_need_update = false;

        if (count($mw_update_attributes) > 0) {

            $mw_update_data['attributes'] = $mw_update_attributes;
            $mw_need_update = true;

        }

		// Если текущий статус СДЭКа -- Доставлен 
        if ($cdek_status['code']===MonitorProcessValues::ORDER_STATUS_DELIVERED) {

            if ($mw_status_uuid == MonitorProcessValues::MW_ORDER_STATUS_DELIVERY) {

                $mw_update_data['state'] = [
                        'meta'=>[
                            'href'=>OrderProcessValues::MW_ORDER_CHANGE_STATE_URL . OrderProcessValues::MW_ATTR_CDEK_ORDER_DELIVERED,
                            'type'=>'state',
                            'mediaType'=>'application/json'
                        ]
                ];

                $mw_need_update = true;

            }

        }

        if ($mw_need_update) {
            $this->mw_connector->updateDataQuery(OrderProcessValues::MW_ORDER_CHANGE_URL.$order->mw_order_uuid, $mw_update_data);
            return "OK ({$cdek_status['code']} @ {$cdek_status['date']})";
        }

        return "NOT CHANGED ({$cdek_status['code']} @ {$cdek_status['date']})";
    }

    /**
     * Находим подходящий доп статус МС для статуса СДЭКа
     * @param string $cdek_status_code  Статус СДЭК
     * @return string  Соответствующий статус МС либо false
     */
    protected function getMwDeliveryStatus($cdek_status_code)
    {
        // Загружаем список статусов ТК из МС (один раз!) и кэшируем на будущее
        if (!$this->mw_delivery_statuses) {
            $mw_status_data = $this->mw_connector->sendSimpleQuery(MwProcessValues::GET_ENTITY_INFO_URL.OrderProcessValues::MW_ENTITY_DELIVERY_COMPANY_STATUSES)["rows"];
            $this->mw_delivery_statuses = collect($mw_status_data)->keyBy('description');
        }

        // Добавляем к статусу СДЭК префикс "СДЭК_", потому что аналогично добавлено в МС
        // (чтобы цифровые коды статусов СДЭК не путались с другими цифровыми статусами)
        return \Arr::get($this->mw_delivery_statuses, 'CDEK_' . $cdek_status_code, false);
    }

}
