<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Order\Services\OrderService;
use Modules\Order\Services\MonitorService;
use Modules\Order\Services\CalculateService;
use Modules\MyWarehouse\Services\MwService;

class OrderController extends Controller
{
    protected $order_service;

    protected $mw_service;

    protected $monitor_service;

    protected $calculate_service;

    public function __construct(OrderService $order_service, MwService $mw_service, MonitorService $monitor_service,
                                CalculateService $calculate_service)
    {
        $this->order_service = $order_service;
        $this->mw_service = $mw_service;
        $this->monitor_service = $monitor_service;
        $this->calculate_service = $calculate_service;
    }

    /**
     * Функция регистрации заказа в системе Cdek
     * @param  Request  $request  Объект HTTP-запроса
     * @return  Product  Объект DTO в котором данные полученные от iiko для нужного продукта
     */
    public function create(Request $request)
    {
        $mw_order_uuid = $request->input("mw_order_uuid");
        $mwOrderData = $this->mw_service->getMwOrderData($mw_order_uuid);
        return $this->order_service->createOrder($mwOrderData);
    }

    /**
     * Действие получение данных о сформированном в СДЕК заказе
     * @param string $cdek_order_uuid
     */
    public function get(string $cdek_order_uuid)
    {
        return $this->order_service->getCdekOrderInfo($cdek_order_uuid);
    }

    /**
     * Действие проверки изменения стоимости доставки через некоторое время (1 день и дальше)
     */
    public function monitorDeliveryCost()
    {
        return $this->monitor_service->checkOrderDeliveryCostStates();
    }

    /**
     * Действие проверки изменения стоимости доставки через некоторое время (1 день и дальше)
     */
    public function monitorStatuses()
    {
        return $this->monitor_service->checkOrderStatuses();
    }

    /**
     * Действие выполнения расчета стоимости доставки
     */
    public function calculateDeliveryCost(Request $request)
    {
        $request_params = $request->all();
        return $this->calculate_service->calculateDeliveryCostOnTariff($request_params);
    }
}
