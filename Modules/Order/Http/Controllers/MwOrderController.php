<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Order\Repositories\OrderRepository;
use Modules\Order\Services\OrderService;
use Modules\Order\Services\MonitorService;
use Modules\Order\Services\ReportService;

class MwOrderController extends Controller
{
    protected $order_repository;

    protected $order_service;

    protected $monitor_service;

    protected $report_service;

    public function __construct(OrderRepository $order_repository, OrderService $order_service,
                                MonitorService $monitor_service, ReportService $report_service)
    {
        $this->order_repository = $order_repository;
        $this->order_service = $order_service;
        $this->monitor_service = $monitor_service;
        $this->report_service = $report_service;
    }

    /**
     * Получение информации о заказе от СДЭК по UUID МоегоСклада
     * @param string $mw_order_uuid
     */
    public function get(string $mw_order_uuid)
    {
        $order = $this->order_repository->getOrderByMwId($mw_order_uuid);
        return $this->order_service->getCdekOrderInfo($order->cdek_order_uuid);
    }

    /**
     * Обновление статуса заказа по UUID МоегоСклада
     * @param string $mw_order_uuid
     */
    public function check(string $mw_order_uuid)
    {
        $order = $this->order_repository->getOrderByMwId($mw_order_uuid);
        return $this->monitor_service->checkOrderStatus($order);
    }

    /**
     * Ручной запуск скачивания этикетки и накладной на случай если не сработал стандартный метод
     * @param string $cdek_order_uuid
     */
    public function process(string $mw_order_uuid)
    {
        $order = $this->order_repository->getOrderByMwId($mw_order_uuid);

        $cdek_order_info = $this->order_service->getCdekOrderInfo($order->cdek_order_uuid);
        $cdek_number = \Arr::get($cdek_order_info, 'entity.cdek_number');

        $this->report_service->formDeliveryNote($order->cdek_order_uuid, $cdek_number);
        $this->report_service->formBarCode($order->cdek_order_uuid, $cdek_number);

        return response()->json(['cdek_number'=>$cdek_number, 'success'=>true]);
    }

}
