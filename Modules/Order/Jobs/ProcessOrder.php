<?php

namespace Modules\Order\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\Order\Services\OrderService;
use Modules\Order\Services\OrderReportService;
use Modules\Order\Exceptions\CustomCdekOrderCreateException;

class ProcessOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $mw_order_uuid;
	protected $cdek_order_uuid;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $mw_order_uuid, string $cdek_order_uuid)
    {
        $this->mw_order_uuid = $mw_order_uuid;
        $this->cdek_order_uuid = $cdek_order_uuid;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OrderService $order_service, OrderReportService $order_report_service)
    {
        $cdek_order_info = $order_service->getCdekOrderInfo($this->cdek_order_uuid);
        $cdek_number = \Arr::get($cdek_order_info, 'entity.cdek_number');

        if (!$cdek_number) {
            // В ответе может быть указано несколько ошибок, из-за которых заказ не был принят
            // Объединяем их все в одну строку для передачи в "Ошибки интеграции"
            $cdek_error = \Arr::get($cdek_order_info, 'requests.0.errors', [
                [
                    'message' => 'Ошибка при создании заказа СДЭК'
                ]
            ]);
            $cdek_message = collect($cdek_error)->pluck('message')->join('; ');

            // Здесь нужна именно такая форма, $exception->report() вместо throw $exception
            // Потому что это происходит внутри job-а
            // При throw $exception не происходит report() и сообщение не доходит до "Ошибок интеграции"
            $exception = new CustomCdekOrderCreateException($cdek_message, 0, null, $this->mw_order_uuid);
            $exception->report();
        }

        $order_report_service->doFinalActions($this->mw_order_uuid, $this->cdek_order_uuid, $cdek_number);
    }
}
