<?php

namespace Modules\Order\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\Order\Services\OrderReportService;

class SaveDeliveryNote implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $cdek_delivery_note_uuid;
	protected $cdek_order_number;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $cdek_delivery_note_uuid, string $cdek_order_number)
    {
        $this->cdek_delivery_note_uuid = $cdek_delivery_note_uuid;
        $this->cdek_order_number = $cdek_order_number;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OrderReportService $order_report_service)
    {
        // @TODO: Добавить проверку что файл действительно скачался
        $order_report_service->saveDeliveryNote($this->cdek_delivery_note_uuid, $this->cdek_order_number);
    }
}
