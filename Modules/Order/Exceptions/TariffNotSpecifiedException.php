<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\CdekOrderCreatingException;

class TariffNotSpecifiedException extends CdekOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'В заказе не указан конкретный тариф доставки для ТК';
    }
}
