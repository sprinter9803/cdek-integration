<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\CdekOrderCreatingException;

class ErrorCdekOrderCreateException extends CdekOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Возникли ошибки при создании заказа СДЕК';
    }
}
