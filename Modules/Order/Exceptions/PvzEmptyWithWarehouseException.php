<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\CdekOrderCreatingException;

class PvzEmptyWithWarehouseException extends CdekOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'В заказе не указан ПВЗ с учетом тарифа доствки до склада';
    }
}
