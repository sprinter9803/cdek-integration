<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\CdekOrderCreatingException;

class SellerNotSpecifiedException extends CdekOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'В заказе не указаны данные о продавце';
    }
}
