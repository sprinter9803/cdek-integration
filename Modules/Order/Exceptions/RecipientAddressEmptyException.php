<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\CdekOrderCreatingException;

class RecipientAddressEmptyException extends CdekOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Адрес населенного пункта не заполнен';
    }
}
