<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\CdekOrderCreatingException;

class PositionsNotExistException extends CdekOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'В заказе нету ни одной позиции, которую можно использовать в посылке';
    }
}
