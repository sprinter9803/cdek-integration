<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\CdekOrderCreatingException;

class CustomCdekOrderCreateException extends CdekOrderCreatingException
{
    protected $message;

    public function __construct($message="", $code=0, Exception $previous=null, $mw_order_uuid)
    {
        $this->message = $message;
        parent::__construct($message, $code, $previous, $mw_order_uuid);
    }

    protected function specifiedTypeMessage()
    {
        return $this->message;
    }
}
