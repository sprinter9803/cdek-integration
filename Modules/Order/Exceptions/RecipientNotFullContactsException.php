<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\CdekOrderCreatingException;

class RecipientNotFullContactsException extends CdekOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'В заказе у получателя не указаны полностью данные телефона и email-а';
    }
}
