<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'order'], function () {
    Route::post('create', 'OrderController@create')->name('order.create');
    Route::get('get/{mw_order_uuid}', 'OrderController@get')->name('order.get');

    Route::get('create', 'OrderController@create');
    Route::get('info/{mw_order_uuid}', 'MwOrderController@info')->name('order.info');
    Route::get('check/{mw_order_uuid}', 'MwOrderController@check')->name('order.check');
    Route::get('process/{mw_order_uuid}', 'MwOrderController@process')->name('order.process');
});

Route::group(['prefix' => 'monitor'], function () {
    Route::get('delivery_cost', 'OrderController@monitorDeliveryCost')->name('monitor.delivery_cost');
    Route::get('order_statuses', 'OrderController@monitorStatuses')->name('monitor.statuses');
});

Route::group(['prefix' => 'calculate'], function () {
    Route::post('delivery_cost', 'OrderController@calculateDeliveryCost')->name('calculate.delivery_cost');
});
