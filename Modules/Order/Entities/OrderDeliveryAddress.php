<?php

namespace Modules\Order\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения данных об адресе доставки
 *
 * @author Oleg Pyatin
 */
class OrderDeliveryAddress extends BaseDto
{
    /**
     * @var string  Индекс адреса доставки
     */
    public $index;
    /**
     * @var string  Улица адреса доставки
     */
    public $street;
    /**
     * @var string  Дом адреса доставки
     */
    public $house;
    /**
     * @var string  Строение/корпус адреса доставки
     */
    public $structure;
    /**
     * @var string  Квартира адреса доставки
     */
    public $flat;
}
