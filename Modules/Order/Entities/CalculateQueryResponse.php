<?php

namespace Modules\Order\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения ответа по расчету стоимости от API СДЕК
 *
 * @author Oleg Pyatin
 */
class CalculateQueryResponse extends BaseDto
{
    /**
     * @var Стоимость доставки
     */
    public $delivery_sum;
    /**
     * @var Минимальное время доставки (в рабочих днях)
     */
    public $period_min;
    /**
     * @var Максимальное время доставки (в рабочих днях)
     */
    public $period_max;
    /**
     * @var Расчетный вес (в граммах)
     */
    public $weight_calc;
    /**
     * @var Стоимость доставки с учетом доп.услуг
     */
    public $total_sum;
    /**
     * @var Валюта в которой рассчитывается стоимость доставки
     */
    public $currency;
    /**
     * @var Список ошибок (если есть)
     */
    public $errors;
}
