<?php

namespace Modules\Order\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения данных для создания заказа в системе СДЕК
 *
 * @author Oleg Pyatin
 */
class OrderCreateData extends BaseDto
{
    /**
     * @var Код тарифа
     */
    public $tariff_code;
    /**
     * @var Получатель
     */
    public $recipient;
    /**
     * @var Адрес отправления
     */
    public $from_location;
    /**
     * @var Адрес получения
     */
    public $to_location;
    /**
     * @var Список информации по местам (упаковкам)
     */
    public $packages;
    /**
     * @var Данные отправителя
     */
    public $sender;
    /**
     * @var Комментарий к заказу
     */
    public $comment;
    /**
     * @var Имя-номер заказа (Номер заказа в ИС Клиента)
     */
    public $number;
    /**
     * @var Данные настоящего продавца
     */
    public $seller;
    /**
     * @var Код ПВЗ куда будет доставлена посылка
     */
    public $delivery_point;
    /**
     * @var Дополнительные услуги
     */
    public $services;
    /**
     * @var Данные сбора за доставку
     */
    public $delivery_recipient_cost;
}
