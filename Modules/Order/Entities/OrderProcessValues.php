<?php

namespace Modules\Order\Entities;

/**
 * Класс для хранения вспомогательной информации к работе с заказами
 *
 * @author Oleg Pyatin
 */
class OrderProcessValues
{
    /**
     * Текущий URL для создания заказа
     */
    public const CDEK_ORDER_CREATE_URL = 'https://api.cdek.ru/v2/orders';
    /**
     * URL для получения данных о списке ПВЗ
     */
    public const CDEK_DELIVERY_POINT_LIST = 'https://api.cdek.ru/v2/deliverypoints';
    /**
     * URL для создания накладной квитанции
     */
    public const CDEK_DELIVERY_NOTE_CREATE = 'https://api.cdek.ru/v2/print/orders';
    /**
     * URL для получения накладной квитанции
     */
    public const CDEK_DELIVERY_NOTE_GET = 'https://api.cdek.ru/v2/print/orders/';
    /**
     * URL для получения накладной квитанции
     */
    public const CDEK_DELIVERY_NOTE_FILE_GET = 'http://api.cdek.ru/v2/print/orders/';
    /**
     * URL для получения заказа из системы СДЕК
     */
    public const CDEK_ORDER_GET_URL = 'https://api.cdek.ru/v2/orders/';
    /**
     * URL формирования ШК места к заказу
     */
    public const CDEK_BAR_CODE_CREATE = 'https://api.cdek.ru/v2/print/barcodes';
    /**
     * URL получения ШК места к заказу (файл доступен только в течение часа)
     */
    public const CDEK_BAR_CODE_GET = 'https://api.cdek.ru/v2/print/barcodes/';


    /**
     * URL для изменения заказов в МойСклад
     */
    public const MW_ORDER_CHANGE_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/';
    /**
     * URL для изменения статусов заказов в МойСклад
     */
    public const MW_ORDER_CHANGE_STATE_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata/states/';
    /**
     * URL для изменения атрибутов заказа в МойСклад (например указания ошибок, накладной ТК и пр)
     */
    public const MW_ORDER_CHANGE_ATTR_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata/attributes/';


    /**
     * Константа обозначающая тип заказа ИМ
     */
    public const ORDER_INT_SHOP_TYPE = 1;
    /**
     * Константа обозначающая тип заказа Доставка
     */
    public const ORDER_DELIVERY_TYPE = 2;
    /**
     * Комментарий для заказа по-умолчанию
     */
    public const DEFAULT_PACKAGE_COMMENT = 'Позвонить за 1 час. Стандартная доставка';
    /**
     * Код Москвы в системе СДЕК (нужен для указания адреса отправки)
     */
    public const SENDER_MOSCOW_REGION_CDEK_CODE = "44";
    /**
     * Фиксированный адрес отправки
     */
    public const SENDER_ADDRESS_STRING = "Москва. ул Малая Калужская 15, стр 17";


    /**
     * UUID атрибута выбранного тарифа
     */
    public const TARIFF_ATTR_UUID = 'ebba2610-8a77-11e8-9109-f8fc0005e497';
    /**
     * UUID атрибута продавца
     */
    public const ATTR_SELLER_UUID = '3b22f40e-5461-11e9-912f-f3d4001887f6';
    /**
     * UUID атрибута комментария
     */
    public const ATTR_COMMENT_UUID = '9ffdbf2e-45ac-11e5-7a40-e89700218f1a';


    /**
     * UUID атрибута длины
     */
    public const ATTR_ASSORT_LENGTH = '52e3ea13-ecbd-11e8-9107-50480007f4fa';
    /**
     * UUID атрибута ширины
     */
    public const ATTR_ASSORT_WIDTH = '52e3ed5a-ecbd-11e8-9107-50480007f4fb';
    /**
     * UUID атрибута высоты
     */
    public const ATTR_ASSORT_HEIGHT = '52e3ef2b-ecbd-11e8-9107-50480007f4fc';


    /**
     * Значение НДС для посылки по-умолчанию (В API 1.5 было VATX - тип string, в новом (2.0) тип int
     *      поэтому ставим 0)
     */
    public const VAT_DEFAULT_VALUE = 0;//'VATX';

    /**
     * Аттрибут обозначения что заказ уже оплачен
     */
    public const ATTR_ORDER_IS_PAYED = 'b3417c57-89dd-11e8-9109-f8fc0039ea47';

    /**
     * Вес посылки по-умолчанию (полкилограмма) если не указан
     */
    public const PACKAGE_DEFAULT_WEIGHT = 500;
    /**
     * Ограничение на длину идентификатора/артикула товара в системе СДЕК
     */
    public const WARE_KEY_LENGTH_LIMIT = 49;

    /**
     * Название аттрибута для кода города
     */
    public const ATTR_RECIPIENT_CITY = '23e9ff7e-8a86-11e8-9109-f8fc00076eaf';

    /**
     * Атрибут для получения значения ПВЗ
     */
    public const ATTR_PVZ_CODE = '750d08d4-4340-11ea-0a80-02f700046652';
    /**
     * Временный тестовый ПВЗ для случая тестовой четной записи СДЭК (база ПВЗ здесь в 3 раза меньше)
     */
    public const TEST_PVZ_CODE = 'SML4';
    /**
     * Основной почтовый код места отправки
     */
    public const SEND_CITY_DEFAULT_POST_CODE = '107076';
    /**
     * UUID атрибута дополнительного сервиса страховки груза
     */
    public const ATTR_SERVICE_SAFE_DELIVERY = 'dff650cf-89dd-11e8-9109-f8fc003a7cff';
    /**
     * Обозначение кода услуги страховки груза (в старой версии 1.5 было просто 2)
     */
    public const SERVICE_SAFE_DELIVERY_MARK = 'INSURANCE';
    /**
     * UUID атрибута дополнительного сервиса страховки груза
     */
    public const ATTR_SERVICE_INSPECT_DELIVERY = '5a132c93-55e5-11e9-9ff4-3150000e54a3';
    /**
     * Обозначение кода услуги страховки груза (в старой версии 1.5 было просто 2)
     */
    public const SERVICE_INSPECT_DELIVERY_MARK = 'INSPECTION_CARGO';


    /**
     * UUID атрибута адреса покупателя
     */
    public const ATTR_RECIPIENT_MW_ORDER_ADDR = '2a5b2325-49cf-11e6-7a69-97110010a6c2';
    /**
     * UUID атрибута индекса адреса доставки
     */
    public const ATTR_RECIP_ADDR_INDEX = '4919ebd0-545c-11e9-912f-f3d400170650';
    /**
     * UUID атрибута улицы адреса доставки
     */
    public const ATTR_RECIP_ADDR_STREET = '68670058-545d-11e9-9109-f8fc001790e0';
    /**
     * UUID атрибута дома адреса доставки
     */
    public const ATTR_RECIP_ADDR_HOUSE = 'edfc2eef-545d-11e9-9109-f8fc00185b5f';
    /**
     * UUID атрибута строения адреса доставки
     */
    public const ATTR_RECIP_ADDR_STRUCTURE = '2e4d754a-545e-11e9-912f-f3d400182b17';
    /**
     * UUID атрибута квартиры адреса доставки
     */
    public const ATTR_RECIP_ADDR_FLAT = '6e31d481-545e-11e9-9107-504800182b49';
    /**
     * UUID атрибута города доставки
     */
    public const ATTR_RECIP_ADDR_CITY = '132fa987-545d-11e9-9ff4-34e80017a9fa';

    /**
     * UUID атрибута телефона покупателя в заказе
     */
    public const ATTR_RECIP_PHONE = '9dc79b5e-cb2f-11e3-8493-002590a28eca';
    /**
     * UUID атрибута почты покупателя в заказе
     */
    public const ATTR_RECIP_EMAIL = '9dc79c29-cb2f-11e3-fb29-002590a28eca';


    /**
     * Число копий накладной квитанции к заказу
     */
    public const DELIVERY_NOTE_COPY_COUNT = 4;
    /**
     * Число маркировок к заказу
     */
    public const DELIVERY_BAR_CODE_COUNT = 1;
    /**
     * Число маркировок к заказу
     */
    public const DELIVERY_BAR_CODE_FORMAT = 'A6';
    /**
     * Язык для маркировки
     */
    public const DELIVERY_BAR_CODE_LANG = 'RUS';
    /**
     * Обозначение кода доставки для товаров (указанного в МС)
     */
    public const DELIVERY_COST_CODE = 'dostavka-new';

    /**
     * Обозначение Laravel-хранилища для сохраняемых от CDEK документов
     */
    public const SAVE_CDEK_FILES_DISK = 'pdf';

    /**
     * Атрибут в котором мы указываем номер заказа в системе СДЕК
     */
    public const MW_ATTR_CDEK_ORDER_NUMBER = 'e927642b-88b7-11e7-6b01-4b1d00073fdc';
    /**
     * Ссылка на накладную Транспортной Компании (СДЕК)
     */
    public const MW_ATTR_CDEK_DELIVERY_NOTE = 'cf5373b4-89dd-11e8-9109-f8fc003a7c75';
    /**
     * Ссылка на печать штрихкодов
     */
    public const MW_ATTR_CDEK_BARCODE = 'e9a95f3d-4fa2-11e9-9109-f8fc0003db1e';
    /**
     * Статус заказа "Выгружено в СДЭК"
     */
    public const MW_ATTR_CDEK_ORDER_SUCCESS = 'b0f6b3b4-56ce-11e9-9107-5048001c5753';
    /**
     * Статус заказа "Доставлен"
     */
    public const MW_ATTR_CDEK_ORDER_DELIVERED = '8f6344ed-e1a0-11e8-9ff4-31500021369b';
    /**
     * Атрибут для вывода ошибок
     */
    public const MW_ATTR_ERROR_MESSAGE = 'a31f1635-4fa3-11e9-912f-f3d400039420';
    /**
     * Атрибут для расчета себестоимости доставки
     */
    public const MW_ATTR_DELIVERY_COST = 'cb8c6079-799d-11e5-7a40-e8970024bde7';
    /**
     * Атрибут для установки значения статуса в ТК
     */
    public const MW_ATTR_DELIVERY_COMPANY_STATE = 'c60dfec7-a63d-11ea-0a80-0641000431c7';
    /**
     * Атрибут для установки значения даты изменения статуса в ТК
     */
    public const MW_ATTR_DELIVERY_COMPANY_STATE_CHANGE_DATE = '986c2760-af13-11ea-0a80-05bd0032e5a5';

    /**
     * Код специальной сущности для получения статусов ТК
     */
    public const MW_ENTITY_DELIVERY_COMPANY_STATUSES = '178aea23-a63d-11ea-0a80-00bf000401e9';
}
