<?php

namespace Modules\Order\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения параметров процесса сохранения заказа (используется для заполнения в БД)
 *
 * @author Oleg Pyatin
 */
class OrderFormingParams extends BaseDto
{
    /**
     * @var string   UUID заказа МойСклад
     */
    public $mw_order_uuid;
    /**
     * @var string   Номер обозначения заказа в МойСклад
     */
    public $mw_order_number;
    /**
     * @var string   UUID сформированного заказа CDEK
     */
    public $cdek_order_uuid;
    /**
     * @var string (text в SQL)   Текст запроса в json форме который ушел в СДЕК
     */
    public $cdek_order_request;
    /**
     * @var string (text в SQL)   Текст ответа от СДЕК в json форме
     */
    public $cdek_order_response;
    /**
     * @var string   Статус заказа (возможно будущая функциональность)
     */
    public $status;
}
