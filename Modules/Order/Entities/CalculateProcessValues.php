<?php

namespace Modules\Order\Entities;

/**
 * Класс для хранения вспомогательной информации для действий с расчетами стоимости
 *
 * @author Oleg Pyatin
 */
class CalculateProcessValues
{
    /**
     * URL для выполнения расчета стоимости доставки
     */
    public const CDEK_DELIVERY_COST_CALCULATE_URL = 'https://api.cdek.ru/v2/calculator/tariff';
}
