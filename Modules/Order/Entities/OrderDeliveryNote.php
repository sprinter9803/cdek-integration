<?php

namespace Modules\Order\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения данных о накладной квитанции
 *
 * @author Oleg Pyatin
 */
class OrderDeliveryNote extends BaseDto
{
    /**
     * @var string  Список заказов (в текущему случае 1)
     */
    public $orders;
    /**
     * @var string  Число копий одной квитанции на листе
     */
    public $copy_count;
    /**
     * @var string  Форма квитанции
     */
    public $type;
}
