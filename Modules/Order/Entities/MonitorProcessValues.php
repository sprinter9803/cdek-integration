<?php

namespace Modules\Order\Entities;

/**
 * Класс для хранения вспомогательной информации для процессов мониторинга
 *
 * @author Oleg Pyatin
 */
class MonitorProcessValues
{
    /**
     * Успешное обновление значений стоимости доставки
     */
    public const DELIVERY_COST_SUCCESS_UPDATE = 'Стоимости доставки успешно обновлены для заказов СДЕК';
    /**
     * Успешное обновление статусов доставки
     */
    public const ORDERS_STATUS_SUCCESS_UPDATE = 'Статусы заказов успешно обновлены для заказов СДЕК';

    /**
     * Тип заказа в новом API СДЕК-а (2.0) - Вручен
     */
    public const ORDER_STATUS_DELIVERED = 'DELIVERED';

    /**
     * Состояние заказа в МС - Доставка
     */
    public const MW_ORDER_STATUS_DELIVERY = 'f8ba5948-cb9f-11e3-c3d8-002590a28eca';
}
