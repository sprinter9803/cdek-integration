<?php

namespace Modules\Order\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения данных расчета стоимости доставки
 *
 * @author Oleg Pyatin
 */
class CalculateQueryData extends BaseDto
{
    /**
     * @var Код тарифа
     */
    public $tariff_code;
    /**
     * @var Код тарифа
     */
    public $from_location;
    /**
     * @var Код тарифа
     */
    public $to_location;
    /**
     * @var Код тарифа
     */
    public $packages;
}
