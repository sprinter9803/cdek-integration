<?php

namespace Modules\Order\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения данных о ШК места к заказу
 *
 * @author Oleg Pyatin
 */
class OrderBarCode extends BaseDto
{
    /**
     * @var string  Список заказов (в текущему случае 1)
     */
    public $orders;
    /**
     * @var string  Число копий (по-умолчанию будет равно 1)
     */
    public $copy_count;
    /**
     * @var string  Форма печати
     */
    public $format;
    /**
     * @var string  Язык печатной формы
     */
    public $lang;
}
