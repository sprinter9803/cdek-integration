<?php

namespace Modules\Order\Factories;

use App\Models\OrderLink;
use App\Models\ErrorAttempt;
use Modules\Order\Entities\OrderFormingParams;

/**
 * Класс-фабрика для создания записей в БД о выполняемых действиях
 *
 * @author Oleg Pyatin
 */
class OrderFactory
{
    /**
     * Функция для создания новой связки заказа МойСклад и СДЕК (в случае успешного сохранения)
     * @return OrderLink
     */
    public function createOrderLink(OrderFormingParams $order_forming_params): OrderLink
    {
        $new_order_link = new OrderLink();

        $new_order_link->mw_order_uuid = $order_forming_params->mw_order_uuid;
        $new_order_link->mw_order_number = $order_forming_params->mw_order_number;
        $new_order_link->cdek_order_uuid = $order_forming_params->cdek_order_uuid;

        $new_order_link->cdek_order_request = $order_forming_params->cdek_order_request;
        $new_order_link->cdek_order_response = $order_forming_params->cdek_order_response;

        $new_order_link->status = null;  // Пока просто null, статус может быть для дальнейшей функциональности

        return $new_order_link;
    }

    /**
     * Функция для создания записи об ошибочной попытке с логирующей информацией
     * @param OrderFormingParams $order_forming_params
     */
    public function createErrorAttempt(OrderFormingParams $order_forming_params): ErrorAttempt
    {
        $new_error_attempt = new ErrorAttempt();

        $new_error_attempt->mw_order_uuid = $order_forming_params->mw_order_uuid;
        $new_error_attempt->mw_order_number = $order_forming_params->mw_order_number;
        $new_error_attempt->cdek_order_request = $order_forming_params->cdek_order_request;
        $new_error_attempt->cdek_order_response = $order_forming_params->cdek_order_response;

        return $new_error_attempt;
    }
}
